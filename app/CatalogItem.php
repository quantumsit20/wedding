<?php

namespace App;
use App\Catalog;
use Illuminate\Database\Eloquent\Model;

class CatalogItem extends Model
{
    public function catalog($id){
        $lang = \Lang::getLocale();
        return Catalog::select($lang.'_name as name','id',$lang.'_desc as desc','catalog_img')->where('id',$id)->get()->first();
    }
}
