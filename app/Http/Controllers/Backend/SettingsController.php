<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $setting = Setting::findOrfail(1);
        return view('backend.pages.setting.index',compact('setting'));
    }

    public function update(Request $request)
    {

        $title       = $request->title;
        $description      = $request->description;
        $phone_number     = $request->phone_number;
        $currency     = $request->currency;
        $email     = $request->email;



        $setting = Setting::findOrfail(1);
        $setting ->title = $title;
        $setting->description = $description;
        $setting->phone_number = $phone_number;
        $setting->email = $email;
        $setting->currency = $currency;


        if($request->hasFile('logo')){
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('logo'), $imageName);
            $logo = $imageName;
            $setting->logo = $logo;


        }

        $setting->save();

        return back();
    }
}
