<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use App\Catalog;
use App\CatalogItem;
use App\Inventory;
use App\Order_Review;
use App\Task;

use Auth;
use DB;

class OrdersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $orders = Order::all();
        return view('backend.pages.orders.index',compact('orders'));
    }

  
    public function create()
    {
        if(!Auth::user()->hasPermissionTo('create_orders'))
            abort(403);
        $customers = Customer::all();
        $lang = \Lang::getLocale();
        $catalogs = Catalog::select($lang.'_name as name','id','catalog_img',$lang.'_desc as desc','categories_id')->get();
       
        return view('backend.pages.orders.create',compact('customers','catalogs'));
    }

    public function customers(Request $request,$id){
        if($request->ajax()){
            $customers = Customer::findOrfail($id);
            return response()->json(['customers'=>$customers]);
        }
    }
    
    public function catalogs(Request $request,$id){
        if($request->ajax()){
            $catalog_total = DB::select('SELECT SUM(total_price) AS "total" FROM catalog_items WHERE cataglog_id = '.$id)[0]->total;
            return response()->json(['catalog_total'=>$catalog_total]);
        }
    }

   
    public function store(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create_orders'))
            abort(403);

        $request->validate([
            'company' => 'required|max:255|min:2',
            'address' => 'required|max:255|min:2',
            'order_day' => 'required',
            'order_from' => 'required',
            'order_to' => 'required',
            'no_attendance' => 'required',
        ]);

        $data = [];
        $order_data = [];
        
        $customer = Customer::findOrfail($request->customer_id);

        $order = new Order();
        $order->order_code = 'ORDER_'.date('Y').rand(1,99);
        $order->customer_id = $request->customer_id;
        $order->company = $request->company;
        $order->address = $request->address;
        $order->order_day = $request->order_day;
        $order->order_from = $request->order_from;
        $order->order_to = $request->order_to;
        $order->no_attendance = $request->no_attendance;
        $order->total_price = $request->amount;
        $descs = explode(',',$request->descriptions);

        

        for ($i=0; $i < count($descs); $i++) {
            $desc = explode('@',$descs[$i]);
            $order_data['id_'.$i] = $desc[0];
            $order_data['title_'.$i] = $desc[1];
            $order_data['quantity_'.$i] = $desc[2];
            $order_data['total_'.$i] = $desc[3];

            $item = CatalogItem::findOrfail($desc[0]);
            $inventory = Inventory::findOrfail($item->inventory_id);
            $inventory->quantity = $inventory->quantity - $desc[2];
            $inventory->save();
        }

        $order->order_data = json_encode($order_data);
        $customer->status = 1;
        $customer->save();
        
        if($request->follow_name != null){
            for ($i=0; $i < count($request->follow_name); $i++) { 
                $data['follow_name_'.$i] = $request->follow_name[$i];
                $data['follow_mobile_'.$i] = $request->follow_mobile[$i];
                $data['follow_email_'.$i] = $request->follow_email[$i];
            }
    
            $order->followers = json_encode($data);
            $order->save();
            
        }else{
            $order->save();
        }


        return redirect()->route('orders')->with('success','');
    }

  
    public function show($id)
    {
        if(!Auth::user()->hasPermissionTo('show_orders'))
            abort(403);
        $orders = Order::findOrfail($id);
        $customer = Customer::findOrfail($orders->customer_id);
        return view('backend.pages.orders.show',compact('orders','customer'));
    }

    public function edit($id)
    {
        if(!Auth::user()->hasPermissionTo('edit_orders'))
            abort(403);
        
        if(Task::where('order_id',$id)->get()->count() > 0)
            abort(403);
        $order = Order::findOrfail($id);
        $customers = Customer::all();
        $lang = \Lang::getLocale();
        $checkCat = DB::select('SELECT catalogs.id AS "id" FROM `catalogs` 
                                INNER JOIN `catalog_items` ON `catalog_items`.`cataglog_id` = `catalogs`.`id` 
                                INNER JOIN `inventories` ON `inventories`.`id` = `catalog_items`.`inventory_id` 
                                WHERE `inventories`.`quantity` = 0');
        $cat_array = [];
        foreach ($checkCat as $cat) {
            array_push($cat_array,$cat->id);
        }
        $catalogs = Catalog::select(DB::raw('DISTINCT(catalogs.id) as id'),'catalogs.'.$lang.'_name as name','catalogs.catalog_img','catalogs.'.$lang.'_desc as desc')
                            ->join('catalog_items','catalog_items.cataglog_id','catalogs.id')
                            ->join('inventories','inventories.id','catalog_items.inventory_id')->whereNotIn('catalogs.id',$cat_array)->get();
        
        return view('backend.pages.orders.edit',compact('customers','catalogs','order'));
    }

    public function update(Request $request, $id)
    {
        if(!Auth::user()->hasPermissionTo('edit_orders'))
            abort(403);
        if(Task::where('order_id',$id)->get()->count() > 0)
            abort(403);
        
        $request->validate([
            'company' => 'required|max:255|min:2',
            'address' => 'required|max:255|min:2',
            'order_day' => 'required',
            'order_from' => 'required',
            'order_to' => 'required',
            'no_attendance' => 'required',
        ]);

        $data = [];

        $order = Order::findOrfail($id);
        $order->company = $request->company;
        $order->address = $request->address;
        $order->order_day = $request->order_day;
        $order->order_from = $request->order_from;
        $order->order_to = $request->order_to;
        if ($request->catalog_id == null) {
            $order->catalog_id = $request->catalogs;
        }else{
            $order->catalog_id = $request->catalog_id;
        }
        $order->no_attendance = $request->no_attendance;
        $order->total_price = 0;

        
        
        if($request->follow_name != null){
            for ($i=0; $i < count($request->follow_name); $i++) { 
                $data['follow_name_'.$i] = $request->follow_name[$i];
                $data['follow_mobile_'.$i] = $request->follow_mobile[$i];
                $data['follow_email_'.$i] = $request->follow_email[$i];
            }
    
            $order->followers = json_encode($data);
            $order->save();
    
        }else{
            $order->save();
        }

        $catalog_total = DB::select('SELECT SUM(total_price) AS "total" FROM catalog_items WHERE cataglog_id = '.$order->catalog_id)[0]->total;

        $orderUpdateTotal = Order::findOrfail($order->id);
        $orderUpdateTotal->total_price = $catalog_total;
        $orderUpdateTotal->save();

        if ($request->catalog_id != null) {
            $catalog_items = CatalogItem::where('cataglog_id',$order->catalog_id)->get();

            foreach ($catalog_items as $item) {
                $inventory = Inventory::findOrfail($item->inventory_id);
                $inventory->quantity = $inventory->quantity + $item->quantity;
                $inventory->save();          
            }
        }

        return redirect()->route('orders')->with('success','');
        
    }

    public function destroy($id)
    {
        if(!Auth::user()->hasPermissionTo('delete_orders'))
            abort(403);
        if(Task::where('order_id',$id)->get()->count() > 0)
            abort(403);
        $inventory = Inventory::findOrfail($id);
        $inventory->delete();

        return redirect()->route('inventory')->with('success','');
    }


    public function reviews($id)
    {
        
        $orders = Order::findOrfail($id);
        $reviews = Order_Review::where('order_id','=', $orders->id)
            ->join('orders','orders.id', '=', 'order__reviews.order_id')
            ->get(array('orders.id as id','order__reviews.order_id as order_id','order__reviews.rate as rate','order__reviews.comment as comment'));
        return view('backend.pages.reviews.index',compact('reviews'));
    }

    public function profits()
    {
        if(!Auth::user()->hasPermissionTo('show_profits'))
            abort(403);
        
        $lang = \Lang::getLocale();
        $profits = DB::select('SELECT SUM(inventories.total_price) AS "price", SUM(inventories.total_orignal_price) AS "orignal_price", SUM(inventories.total_price) - SUM(inventories.total_orignal_price) AS "profit", MONTH(orders.order_day) AS "month", YEAR(orders.order_day) AS "year" FROM `inventories` INNER JOIN catalog_items ON catalog_items.inventory_id = inventories.id INNER JOIN catalogs ON catalogs.id = catalog_items.cataglog_id INNER JOIN orders ON orders.catalog_id = catalogs.id GROUP BY MONTH(orders.order_day)');
        
        return view('backend.pages.profits.index',compact('profits'));
    }



}
