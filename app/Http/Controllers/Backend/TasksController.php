<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Task;
use App\Order;
use App\Customer;
use App\Catalog;
use App\CatalogItem;
use App\User;
use App\Department;
use App\UserDeartment;
use Auth;
use DB;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tasks = Task::all();
        return view('backend.pages.tasks.index',compact('tasks'));
    }

  
    public function create()
    {
        if(!Auth::user()->hasPermissionTo('create_tasks'))
            abort(403);
        $lang = \Lang::getLocale();
        $customers = Customer::all();
        $users = User::all();
        $orders = Order::all();
        $departments = Department::select($lang.'_name as name','id')->get();
        
        return view('backend.pages.tasks.create',compact('customers','users','orders','departments'));
    }

    public function orders(Request $request,$id){
        if($request->ajax()){
            $orders = Order::findOrfail($id);
            return response()->json(['orders'=>$orders]);
        }
    }
    
    public function catalogs(Request $request,$id){
        if($request->ajax()){
            $lang = \Lang::getLocale();
            $catalogs = Catalog::select($lang.'_name as name','id','catalog_img',$lang.'_desc as desc')->where('id',$id)->get()->first();
            return response()->json(['catalogs'=>$catalogs]);
        }
    }

    public function catalogsItems(Request $request,$id){
        if($request->ajax()){
            $lang = \Lang::getLocale();
            $ordersdata = Order::findOrfail($id);
            return response()->json(['ordersdata'=>$ordersdata]);
        }
    }

    public function customers(Request $request,$id){
        if($request->ajax()){
            $customers = Customer::where('id',$id)->get()->first();
            return response()->json(['customers'=>$customers]);
        }
    }

    public function users(Request $request,$id){
        if($request->ajax()){
            $lang = \Lang::getLocale();
            $users = User::select('users.name as name','users.id')->join('user_deartments','user_deartments.user_id','users.id')->where('user_deartments.department_id',$id)->get();
            return response()->json(['users'=>$users]);
        }
    }

    public function departments(Request $request,$id){
        if($request->ajax()){
            $lang = \Lang::getLocale();
            $departments = User::select('users.name as name','users.id')->join('user_deartments','user_deartments.user_id','users.id')->where('user_deartments.department_id',$id)->get();
            dd($departments);
            return response()->json(['departments'=>$departments]);
        }
    }

   
    public function store(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create_tasks'))
            abort(403);

        $request->validate([
            'task_title' => 'required|max:255|min:2',

        ]);

        $data = [];

        $task = new Task();

        $orders = Order::findOrfail($request->order_id);

        $task->customer_id = Customer::findOrfail($orders->customer_id)->id;
        $task->user_id = $request->user_id;
        $task->order_id = $request->order_id;
        $task->catalog_id = Catalog::findOrfail($orders->catalog_id)->id;
        $task->task_title = $request->task_title;
        $task->department_id = $request->department_id;
        $task->task_date = $request->task_date;
        $task->status = $request->status;
        if ($request->notes != null)
            $task->notes = $request->notes;
        
        $task->save();

        return redirect()->route('tasks')->with('success','');
    }

  
    public function show($id)
    {
        if(!Auth::user()->hasPermissionTo('show_tasks'))
            abort(403);
        $tasks = Task::findOrfail($id);
        return view('backend.pages.tasks.show',compact('tasks'));
    }


    public function destroy($id)
    {
        if(!Auth::user()->hasPermissionTo('delete_tasks'))
            abort(403);
        $tasks = Task::findOrfail($id);
        $tasks->delete();

        return redirect()->route('tasks')->with('success','');
    }
}
