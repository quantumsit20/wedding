<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\SoicalMedia;
use App\User;
use App\Customer;
use App\Catalog;
use App\Department;
use App\CatalogItem;
use App\Inventory;
use App\Order;
use App\Task;
use App\ContactUs;
use App\Category;
use App\Cart;
use App\Order_Review;
use Session;
use DB;
use Hash;
use Auth;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;

class FrontendController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(
            'changeOrderQuantity',
            'register',
            'index',
            'aboutus',
            'services',
            'single_services',
            'portfolio',
            'contactus',
            'store_contactus',
            'addToCart',
            'viewCart',
            'viewCart',
            'changeOrderQuantity'
        );
    }

    public function changeOrderQuantity($order_date){ //Change Orders Quantity After time is expired
        date_default_timezone_set("Africa/Cairo");
        $date = new DateTime(date('Y-m-d H:i:s'));
        $interval = $date->diff($order_date);
        $days = $interval->format('%a');
        $hours = $interval->format('%h');
        $mins = $interval->format('%i');
        
        if($days == 0){
            return true;
        }else{
            return false;
        }
    }

    public function myAccount(){
        $user_id = Auth::user()->id;
        $customer = Customer::where('user_id',$user_id)->get()->first();
        return view('frontend.pages.account',compact('user_id','customer'));
    }

    public function myAccountPost(Request $request){
        $user_id = Auth::user()->id;
        $user = User::findOrfail($user_id);
        $customer = Customer::where('user_id',$user_id)->get()->first();
        
        if($request->password != null){
            

            $customer->name = $request->name;
            $customer->mobile = $request->mobile;
            $customer->password =  Hash::make($request->password);

            $user->name = $request->name;
            $user->mobile = $request->mobile;
            $user->password =  Hash::make($request->password);

            $user->save();
            $customer->save();
        }else{
            

            $customer->name = $request->name;
            $customer->mobile = $request->mobile;
            $customer->save();

            $user->name = $request->name;
            $user->mobile = $request->mobile;
            $user->password =  Hash::make($request->password);

            $user->save();
        }

        return back()->with('success','');

    }

    public function myorders(){
        $user_id = Auth::user()->id;
        $customer = Customer::where('user_id',$user_id)->get()->first();
        $orders = Order::where('customer_id',$customer->id)->paginate(9);
        return view('frontend.pages.my_orders',compact('user_id','customer','orders'));

    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required|max:255|min:2',
            'mobile' => 'required|max:255|min:2',
            'email' => 'required|unique:customers',
            'password' => 'required|confirmed|min:6|max:20',
        ]);
        
        
        $user = new User();
        $user->mobile = $request->mobile;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        $customers = new Customer();

        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->password = Hash::make($request->password);
        $customers->user_id = $user->id;
        $customers->mobile = $request->mobile;
        $customers->save();
        
        

        if ($request->hasFile('file_path')) {
            for ($i=0; $i <count($request->file_name) ; $i++) { 
                $customersfiles = new CustomerFile();
                $customersfiles->file_name = $request->file_name[$i];

                $image = $request->file('file_path')[$i];
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/customers/files');
                $image->move($destinationPath, $name);
                $customersfiles->file_path = $name;
                $customersfiles->customer_id = $customers->id;
                $customersfiles->save();
            }
            
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('frontend_index');
        }
    }
    
    public function index(){

        $orders = Order::where('orders.status','new')->get();
        
        
        foreach ($orders as $order) {
            $order_date = new DateTime($order->order_day.' '.$order->order_to, new DateTimeZone('Africa/Cairo'));
          
            if ($order->status == 'new') {
                
                if ($this->changeOrderQuantity($order_date)) {
                    $updateOrder = Order::findOrfail($order->id);
                    $updateOrder->status = 'finished';
                    $updateOrder->save();
                    
                    $order_data = json_decode($order->order_data,true);
                    $count = count($order_data) / 4;
                    for ($i=0; $i < $count; $i++) {
                        $id = 'id_'.$i;
                        $quantity = 'quantity_'.$i;
                        $item = CatalogItem::findOrfail($order_data[$id]);
                        $inventory = Inventory::findOrfail($item->inventory_id);
                        $inventory->quantity = $inventory->quantity + $order_data[$quantity];
                        $inventory->save();
                    }
                }
            }
        }


        $lang = \Lang::getLocale();
        $categories = Category::select($lang.'_name as name','id','cat_image',$lang.'_desc as desc','id')->limit(3)->get();
        $catalogs = Catalog::select($lang.'_name as name','id','catalog_img',$lang.'_desc as desc','categories_id')->get();
        $customers_count = Customer::count();
        $orders_count = Order::count();
        $reviews_count = Order_Review::count();
        $catalogs_count = Catalog::count();
        return view('frontend.index',compact('categories','catalogs','customers_count','orders_count','reviews_count','catalogs_count'));
    }
    
    public function aboutus(){
        $lang = \Lang::getLocale();
        $categories = Category::select($lang.'_name as name','id','cat_image',$lang.'_desc as desc','id')->get();
        return view('frontend.pages.aboutus',compact('categories'));
    }
    
    public function services(){
        $lang = \Lang::getLocale();
        $catalogs = Catalog::select($lang.'_name as name','id','catalog_img',$lang.'_desc as desc','categories_id')->get();
        $category = Category::select($lang.'_name as name','id','cat_image',$lang.'_desc as desc','id')->get();
        $items = CatalogItem::select('catalog_items.'.$lang.'_name as name','catalog_items.id','catalog_items.price','catalog_items.quantity','catalog_items.total_price','catalog_items.cataglog_id')
                            ->join('inventories','inventories.id','catalog_items.inventory_id')
                            ->where('inventories.quantity','>',0)->get();
       
        return view('frontend.pages.services',compact('category','catalogs','items'));
    }

    public function single_services($id){
        $lang = \Lang::getLocale();
        $customers = Customer::all();
        $category = Category::select($lang.'_name as name','id','cat_image',$lang.'_desc as desc','id')->get();
        $catalog = Catalog::select($lang.'_name as name','id','catalog_img',$lang.'_desc as desc','categories_id')->where('id',$id)->get()->first();
        $items = CatalogItem::select($lang.'_name as name','id','price','quantity','total_price','cataglog_id')->where('cataglog_id',$catalog->id)->get();
       
        return view('frontend.pages.single_services',compact('category','catalog','items','customers'));
    }

    public function addToCart(Request $request){
        $item = CatalogItem::findOrfail($request->item_id);
        if (Session::has('cart')) {
            $cart = new Cart(Session::get('cart'));
        }else{
            $cart = new Cart();
        }
        $cart->add($item);
        session()->put('cart',$cart);

        return back();
    }

    public function viewCart(){
        if(session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        }else{
            $cart = null;
        }
        return view('frontend.pages.my_cart',compact('cart'));
    }

    public function removeChartItem($id){
        $cart = new Cart(session()->get('cart'));
        $cart->remove($id);

        if ($cart->totalQty <= 0) {
            session()->forget('cart');
        }else{
            session()->put('cart',$cart);
        }
        return back();
    }

    public function checkoutCart(Request $request){
        $orders = [];
        $customers = Customer::all();
        $total_price = 0;
        for ($i=0; $i < count($request->quantity); $i++) { 
            array_push($orders,$request->ids[$i].'@'.$request->title[$i].'@'.$request->quantity[$i].'@'.$request->price[$i]);
            $total_price = $total_price + ($request->quantity[$i] * $request->price[$i]);
        }
        $orders = implode(',',$orders);
        return view('frontend.pages.checkout',compact('total_price','orders','customers'));
    }

    public function chargeCart(Request $request){

        $charge = Stripe::charges()->create([
           'currency' => 'USD',
           'source' =>  $request->stripeToken,
           'amount' => $request->amount,
           'description' => $request->descriptions,
           
        ]);

        $chargeId = $charge['id'];
        if ($chargeId) {
            $data = [];
            $order_data = [];
            
            $customer = Customer::findOrfail($request->customer_id);

            $order = new Order();
            $order->order_code = 'ORDER_'.date('Y').rand(1,99);
            $order->customer_id = $request->customer_id;
            $order->company = $request->company;
            $order->address = $request->address;
            $order->order_day = $request->order_day;
            $order->order_from = $request->order_from;
            $order->order_to = $request->order_to;
            $order->no_attendance = $request->no_attendance;
            $order->total_price = $request->amount;
            $descs = explode(',',$request->descriptions);

            

            for ($i=0; $i < count($descs); $i++) {
                $desc = explode('@',$descs[$i]);
                $order_data['id_'.$i] = $desc[0];
                $order_data['title_'.$i] = $desc[1];
                $order_data['quantity_'.$i] = $desc[2];
                $order_data['total_'.$i] = $desc[3];

                $item = CatalogItem::findOrfail($desc[0]);
                $inventory = Inventory::findOrfail($item->inventory_id);
                $inventory->quantity = $inventory->quantity - $desc[2];
                $inventory->save();
            }

            $order->order_data = json_encode($order_data);
            $customer->status = 1;
            $customer->save();
            
            if($request->follow_name != null){
                for ($i=0; $i < count($request->follow_name); $i++) { 
                    $data['follow_name_'.$i] = $request->follow_name[$i];
                    $data['follow_mobile_'.$i] = $request->follow_mobile[$i];
                    $data['follow_email_'.$i] = $request->follow_email[$i];
                }
        
                $order->followers = json_encode($data);
                $order->save();
                
            }else{
                $order->save();
            }

            session()->forget('cart');
            return redirect()->route('frontend_index')->with('success','');
        }else{
            $customer = Customer::findOrfail($request->customer_id);
            $customer->status = 2;
            $customer->save();
            return back();
        }
    }


    public function view_order($id){
        $order = Order::findOrfail($id);
        $lang = \Lang::getLocale();
        $items = CatalogItem::select($lang.'_name as name','id','price','quantity','total_price','cataglog_id')->where('cataglog_id',$order->catalog_id)->get();
        return view('frontend.pages.view_order',compact('order','items'));
    }

    public function orderRate(Request $request)
    {
        if($request->ajax()){
            $rate_data = $request->rates;
            $order = $request->order;

            $reviews = new Order_Review();
            $reviews->order_id = $order;
            $reviews->comment = $rate_data[0];
            $reviews->rate = $rate_data[1];
            $reviews->save();
        }
    }

    public function services_search(Request $request)
    {
        $lang = \Lang::getLocale();
        $category = Category::select($lang.'_name as name','id','cat_image',$lang.'_desc as desc','id')->get();

        if(isset($request->searchBtn)){
            if($request->search != ''){
                $search = $request->search;
                $catalogs = Catalog::select($lang.'_name as name','id','catalog_img',$lang.'_desc as desc','categories_id');
                $items = CatalogItem::select($lang.'_name as name','id','price','quantity','total_price','cataglog_id')->get();
                $catalogs->where($lang.'_name','like','%'.$request->search.'%')->orWhere($lang.'_desc','like','%'.$request->search.'%')->paginate(9);
                return view('frontend.pages.services_search',compact('category','catalogs','items','search'));
            }
        }

        if(isset($request->filterBtn)){

            if($request->from != '' && $request->to != ''){
                $from = $request->from;
                $to = $request->to;
                $catalogs = DB::select('SELECT catalogs.'.$lang.'_name as name,catalogs.id,catalogs.catalog_img,catalogs.'.$lang.'_desc as desc,catalogs.catalogs_id,SUM(catalog_items.total_price) FROM catalog_items INNER JOIN catalogs ON catalogs.id = catalog_items.cataglog_id GROUP BY catalog_items.cataglog_id HAVING sum BETWEEN '.$request->from.' AND '.$request->from);
                $items = CatalogItem::select($lang.'_name as name','id','price','quantity','total_price','cataglog_id')->get();
                $catalogs->where($lang.'_name','like','%'.$request->search.'%')->orWhere($lang.'_desc','like','%'.$request->search.'%')->paginate(9);
                return view('frontend.pages.services_search',compact('category','catalogs','items','from','to'));
            }

            if($request->from != ''){
                $from = $request->from;
                $catalogs = DB::select('SELECT catalogs.'.$lang.'_name as name,catalogs.id,catalogs.catalog_img,catalogs.'.$lang.'_desc as desc,catalogs.catalogs_id,SUM(catalog_items.total_price) FROM catalog_items INNER JOIN catalogs ON catalogs.id = catalog_items.cataglog_id GROUP BY catalog_items.cataglog_id HAVING sum = '.$request->from);
                $items = CatalogItem::select($lang.'_name as name','id','price','quantity','total_price','cataglog_id')->get();
                $catalogs->where($lang.'_name','like','%'.$request->search.'%')->orWhere($lang.'_desc','like','%'.$request->search.'%')->paginate(9);
                return view('frontend.pages.services_search',compact('category','catalogs','items','from'));
            }

            if($request->to != ''){
                $to = $request->to;
                $catalogs = DB::select('SELECT catalogs.'.$lang.'_name as name,catalogs.id,catalogs.catalog_img,catalogs.'.$lang.'_desc as desc,catalogs.catalogs_id,SUM(catalog_items.total_price) FROM catalog_items INNER JOIN catalogs ON catalogs.id = catalog_items.cataglog_id GROUP BY catalog_items.cataglog_id HAVING sum = '.$request->to);
                $items = CatalogItem::select($lang.'_name as name','id','price','quantity','total_price','cataglog_id')->get();
                $catalogs->where($lang.'_name','like','%'.$request->search.'%')->orWhere($lang.'_desc','like','%'.$request->search.'%')->paginate(9);
                return view('frontend.pages.services_search',compact('category','catalogs','items','to'));
            }
        }

        
                           
       
        
    }
 
    public function portfolio()
    {
        return view('frontend.pages.portfolio');
    }

    public function  contactus()
    {
        return view('frontend.pages.contactus');
    }

    public function  store_contactus(Request $request)
    {
        $this->validate($request, [
            'name'         =>  'required',
            'email'        =>  'required|email',
            'message'      =>  'required',
            'subject'   =>'required'
        ]);

        $data = array(
            'name'      =>  $request->name,
            'message'   =>   $request->message,
            'email'      =>   $request->email,
            'subject'      =>   $request->subject,

        );
        ContactUs::create($data);

        return redirect()->route('frontend_index')->with('success','');

    }
   

}
