<?php

namespace App;
use App\CatalogItem;
use App\Inventory;

class Cart
{
    public $items = [];
    public $totalQty;
    public $totalPrice;

    public function __construct($cart = null){
        if($cart) {
            $this->items = $cart->items;
            $this->totalQty = $cart->totalQty;
            $this->totalPrice = $cart->totalPrice;
        } else {
            $this->items = [];
            $this->totalQty = 0;
            $this->totalPrice = 0;
        }
    }

    public function add($product){
        $item = [
            'id' => $product->id,
            'title' => $product->en_name,
            'price' => $product->price,
            'quantity' => 0,
        ];
        if( !array_key_exists($product->id, $this->items)) {
            $this->items[$product->id] = $item ;
            $this->totalQty +=1;
            $this->totalPrice += $product->price; 
        } else {
            $this->totalQty +=1 ;
            $this->totalPrice += $product->price; 
        }

        $this->items[$product->id]['quantity']  += 1 ;
    }

    public function remove($id){
        if(array_key_exists($id, $this->items)) {
            $this->totalQty = $this->totalQty - $this->items[$id]['quantity'];
            $this->totalPrice = $this->totalPrice - ($this->items[$id]['quantity']*$this->items[$id]['price']);
            unset($this->items[$id]);
        }
    }

    public static function getMaxQty($id){
        $item = CatalogItem::findOrfail($id);
        $inventory = Inventory::findOrfail($item->inventory_id);
        return $inventory->quantity;
    }
}
