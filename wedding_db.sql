-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2020 at 07:31 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedding_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalogs`
--

CREATE TABLE `catalogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalog_img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catalogs`
--

INSERT INTO `catalogs` (`id`, `en_name`, `ar_name`, `en_desc`, `ar_desc`, `catalog_img`, `categories_id`, `created_at`, `updated_at`) VALUES
(49, 'Foods', 'الأطعمة', 'Foods', 'الأطعمة', '1579784899.jpg', 5, '2020-01-23 11:08:19', '2020-01-23 11:08:19'),
(50, 'Drinks', 'المشروبات', 'Drinks', 'المشروبات', '1579785140.jpg', 5, '2020-01-23 11:12:20', '2020-01-23 11:12:20'),
(51, 'Wedding Stands', 'كوشات أفراح', 'Wedding Stands', 'كوشات أفراح', '1579785286.jpg', 5, '2020-01-23 11:14:46', '2020-01-23 11:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `catalog_items`
--

CREATE TABLE `catalog_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cataglog_id` bigint(20) UNSIGNED NOT NULL,
  `inventory_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catalog_items`
--

INSERT INTO `catalog_items` (`id`, `en_name`, `ar_name`, `price`, `quantity`, `total_price`, `cataglog_id`, `inventory_id`, `created_at`, `updated_at`) VALUES
(24, 'Sweets', 'حلوى', '55', '100', '5500', 49, 15, '2020-01-23 11:08:19', '2020-01-23 11:08:19'),
(25, 'Gateau', 'جاتوه', '120', '150', '18000', 49, 20, '2020-01-23 11:08:19', '2020-01-23 11:08:19'),
(26, 'Soft drinks', 'مشروبات غازية', '10', '200', '2000', 50, 21, '2020-01-23 11:12:20', '2020-01-23 11:12:20'),
(27, 'Juices', 'عصائر', '8', '100', '800', 50, 22, '2020-01-23 11:12:20', '2020-01-23 11:12:20'),
(28, 'Wedding Stands', 'كوشة أفراح', '20', '550', '11000', 51, 17, '2020-01-23 11:14:46', '2020-01-23 11:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `en_name`, `ar_name`, `en_desc`, `ar_desc`, `cat_image`, `created_at`, `updated_at`) VALUES
(5, 'Weddings', 'أفراح', 'Weddings', 'أفراح', '1579782987.jpg', '2020-01-23 10:31:53', '2020-01-23 10:36:27'),
(6, 'Special Occasions', 'مناسبات خاصة', 'Special Occasions', 'مناسبات خاصة', '1579783077.png', '2020-01-23 10:33:26', '2020-01-23 10:37:57'),
(7, 'Birthdays', 'أعياد ميلاد', 'Birthdays', 'أعياد ميلاد', '1579783104.jpg', '2020-01-23 10:34:13', '2020-01-23 10:38:24'),
(8, 'Aqeeqah', 'عقيقة', 'Aqeeqah', 'عقيقة', '1579783176.jpg', '2020-01-23 10:34:54', '2020-01-23 10:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed', 'ahmed@ahmed.com', 'aaa', 'aaaaa', '2020-01-25 17:41:25', '2020-01-25 17:41:25'),
(2, 'Admins', 'admin@admin.com', 'aaa', 'aaaaa', '2020-01-25 17:43:44', '2020-01-25 17:43:44'),
(3, 'asd', 'ahmed@ahmed.com', 'aaa', 'aaaaa', '2020-01-25 17:45:52', '2020-01-25 17:45:52');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `mobile`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed', 'ahmed@ahmed.com', NULL, '$2y$10$aP2PBhRxsTvryqIiv/iTf.T86.pcyyQgx/q9OXt2z8DFglB22cUBy', NULL, '11111111111', 1, 10, '2020-01-21 19:44:43', '2020-01-25 18:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `customer_files`
--

CREATE TABLE `customer_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `en_name`, `ar_name`, `created_at`, `updated_at`) VALUES
(1, 'kitchen', 'المطبخ', '2020-01-09 18:26:10', '2020-01-09 18:26:10'),
(2, 'DJ', 'الدى جى', '2020-01-09 19:22:15', '2020-01-09 19:22:15'),
(3, 'Management', 'الادراة', '2020-01-09 19:28:42', '2020-01-09 19:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orignal_price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `total_orignal_price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `en_name`, `ar_name`, `quantity`, `orignal_price`, `price`, `add_value`, `user_id`, `total_orignal_price`, `total_price`, `notes`, `created_at`, `updated_at`) VALUES
(15, 'Sweets', 'حلوى', '102', '50', '55', '0', 9, '5000', '5500', 'Sweets', '2020-01-23 10:26:08', '2020-01-25 20:58:26'),
(16, 'Candles', 'شموع', '100', '20', '22', '0', 9, '2000', '2200', 'Candles', '2020-01-23 10:26:43', '2020-01-23 10:26:43'),
(17, 'Wedding Stands', 'كوشة أفراح', '548', '500', '20', '0', 9, '275000', '11000', 'Wedding Stands', '2020-01-23 10:27:47', '2020-01-25 20:53:56'),
(18, 'Chairs', 'كراسى', '300', '20', '25', '0', 9, '6000', '7500', 'Chairs', '2020-01-23 10:28:29', '2020-01-23 10:28:29'),
(19, 'Tables', 'طاولات', '200', '30', '35', '0', 9, '6000', '7000', 'Tables', '2020-01-23 10:28:58', '2020-01-23 10:28:58'),
(20, 'Gateau', 'جاتوه', '102', '100', '120', '0', 9, '15000', '18000', 'Gateau', '2020-01-23 10:29:50', '2020-01-25 20:53:56'),
(21, 'Soft drinks', 'مشروبات غازية', '199', '8', '10', '0', 9, '1600', '2000', 'Soft drinks', '2020-01-23 11:10:32', '2020-01-25 20:53:56'),
(22, 'Juices', 'عصائر', '99', '6', '8', '0', 9, '600', '800', 'Juices', '2020-01-23 11:11:13', '2020-01-25 14:04:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_05_150242_create_permission_tables', 1),
(5, '2020_01_08_104939_create_user_data_table', 2),
(6, '2020_01_08_110137_create_user_follows_table', 2),
(7, '2020_01_08_194605_create_user_files_table', 2),
(8, '2020_01_08_221525_create_customers_table', 3),
(9, '2020_01_09_190140_create_user_deartments_table', 4),
(10, '2020_01_10_093149_create_catalogs_table', 5),
(11, '2020_01_10_093724_create_catalog_items_table', 6),
(12, '2020_01_10_134047_create_inventories_table', 7),
(13, '2020_01_12_102855_create_orders_table', 8),
(14, '2020_01_14_131924_create_tasks_table', 9),
(15, '2020_01_14_095729_create_contact_us_table', 10),
(16, '2020_01_14_122229_create_settings_table', 10),
(17, '2020_01_14_122321_create_soical_media_table', 10),
(18, '2020_01_16_091611_create_order__reviews_table', 10),
(19, '2020_01_20_204630_create_categories_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 9),
(2, 'App\\User', 9),
(3, 'App\\User', 9),
(4, 'App\\User', 9),
(5, 'App\\User', 9),
(6, 'App\\User', 9),
(7, 'App\\User', 9),
(8, 'App\\User', 9),
(9, 'App\\User', 9),
(10, 'App\\User', 9),
(11, 'App\\User', 9),
(12, 'App\\User', 9),
(13, 'App\\User', 9),
(14, 'App\\User', 9),
(15, 'App\\User', 9),
(16, 'App\\User', 9),
(17, 'App\\User', 9),
(18, 'App\\User', 9),
(19, 'App\\User', 9),
(20, 'App\\User', 9),
(21, 'App\\User', 9),
(22, 'App\\User', 9),
(23, 'App\\User', 9),
(24, 'App\\User', 9),
(25, 'App\\User', 9),
(26, 'App\\User', 9),
(27, 'App\\User', 9),
(28, 'App\\User', 9),
(29, 'App\\User', 9),
(30, 'App\\User', 9),
(31, 'App\\User', 9),
(32, 'App\\User', 9),
(33, 'App\\User', 9),
(34, 'App\\User', 9),
(35, 'App\\User', 9),
(36, 'App\\User', 9),
(37, 'App\\User', 9),
(38, 'App\\User', 9),
(39, 'App\\User', 9),
(40, 'App\\User', 9),
(41, 'App\\User', 9),
(42, 'App\\User', 9);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'place that user want to make party',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `followers` text COLLATE utf8mb4_unicode_ci,
  `order_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_attendance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `customer_id`, `company`, `address`, `order_from`, `order_to`, `order_day`, `followers`, `order_data`, `no_attendance`, `total_price`, `status`, `created_at`, `updated_at`) VALUES
(2, 'ORDER_202080', 1, 'aaa', 'qqq', '17:00', '20:00', '2020-01-25', '{\"follow_name_0\":\"ahmed\",\"follow_mobile_0\":\"01117725721\",\"follow_email_0\":\"ahmed@ahmed.com\",\"follow_name_1\":\"ahmed\",\"follow_mobile_1\":\"01117725721\",\"follow_email_1\":\"ahmed@ahmed.com\"}', '{\"id_0\":\"24\",\"title_0\":\"Sweets\",\"quantity_0\":\"2\",\"total_0\":\"110\",\"id_1\":\"25\",\"title_1\":\"Gateau\",\"quantity_1\":\"2\",\"total_1\":\"120\",\"id_2\":\"28\",\"title_2\":\"Wedding Stands\",\"quantity_2\":\"2\",\"total_2\":\"20\",\"id_3\":\"26\",\"title_3\":\"Soft drinks\",\"quantity_3\":\"2\",\"total_3\":\"10\"}', '0', '410', 'finished', '2020-01-25 14:10:14', '2020-01-25 20:53:56'),
(3, 'ORDER_202049', 1, 'bbb', 'www', '13:30', '20:30', '2020-01-25', '{\"follow_name_0\":\"ahmed\",\"follow_mobile_0\":\"01117725721\",\"follow_email_0\":\"ahmed@ahmed.com\",\"follow_name_1\":\"mohamed\",\"follow_mobile_1\":\"01117725721\",\"follow_email_1\":\"mohamed@mohamed.com\"}', '{\"id_0\":\"24\",\"title_0\":\"Sweets\",\"quantity_0\":\"10\",\"total_0\":\"55\"}', '2', '550', 'finished', '2020-01-25 18:48:28', '2020-01-25 20:53:56'),
(4, 'ORDER_202012', 1, 'aaa', 'qqq', '01:00', '02:00', '2020-01-26', '{\"follow_name_0\":\"ahmed\",\"follow_mobile_0\":\"01117725721\",\"follow_email_0\":\"ahmed@ahmed.com\"}', '{\"id_0\":\"24\",\"title_0\":\"Sweets\",\"quantity_0\":\"1\",\"total_0\":\"55\"}', '0', '55', 'finished', '2020-01-25 18:56:40', '2020-01-25 20:56:40'),
(5, 'ORDER_202053', 1, 'aaa', 'qqq', '01:00', '02:00', '2020-01-25', NULL, '{\"id_0\":\"24\",\"title_0\":\"Sweets\",\"quantity_0\":\"2\",\"total_0\":\"55\"}', '0', '110', 'finished', '2020-01-25 18:58:04', '2020-01-25 20:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `order__reviews`
--

CREATE TABLE `order__reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order__reviews`
--

INSERT INTO `order__reviews` (`id`, `rate`, `comment`, `order_id`, `created_at`, `updated_at`) VALUES
(1, '5', 'aaa', 2, '2020-01-25 20:57:53', '2020-01-25 20:57:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'create_users', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(2, 'edit_users', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(3, 'show_users', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(4, 'delete_users', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(5, 'create_departments', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(6, 'edit_departments', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(7, 'show_departments', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(8, 'delete_departments', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(9, 'create_customers', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(10, 'edit_customers', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(11, 'show_customers', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(12, 'delete_customers', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(13, 'permission_users', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(14, 'department_users', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(15, 'create_catalogs', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(16, 'edit_catalogs', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(17, 'show_catalogs', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(18, 'delete_catalogs', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(19, 'create_items', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(20, 'edit_items', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(21, 'show_items', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(22, 'delete_items', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(23, 'create_inventory', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(24, 'edit_inventory', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(25, 'show_inventory', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(26, 'delete_inventory', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(27, 'create_orders', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(28, 'edit_orders', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(29, 'show_orders', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(30, 'delete_orders', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(31, 'create_tasks', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(32, 'edit_tasks', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(33, 'show_tasks', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(34, 'delete_tasks', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(35, 'check_inventory', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(36, 'show_profits', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(37, 'edit_profiles', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(38, 'create_category', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(39, 'edit_category', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(40, 'show_category', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(41, 'delete_category', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33'),
(42, 'withdraw_inventory', 'web', '2020-01-07 19:19:33', '2020-01-07 19:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `logo`, `description`, `subject`, `phone_number`, `email`, `currency`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'El Fesal', '1579970845.png', 'الفيصل لإدارة عمليات شركة', 'Subject', '01234567891', 'fasel@gmail.com', 'USD', 'Keywords', NULL, '2020-01-25 14:52:49');

-- --------------------------------------------------------

--
-- Table structure for table `soical_media`
--

CREATE TABLE `soical_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `soical_media`
--

INSERT INTO `soical_media` (`id`, `facebook`, `twitter`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com/', 'https://www.twitter.com/', 'https://www.instagram.com/', NULL, '2020-01-25 14:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `catalog_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `task_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(9, 'Admin', 'admin@admin.com', '01117725721', NULL, '$2y$10$dpiFrsXmxekkPQ1Ey8QDzugV7cq8lHgytw1jm2QDADrP1/PUp2RFa', NULL, '2020-01-09 17:31:06', '2020-01-18 11:22:19'),
(10, 'Ahmed', 'ahmed@ahmed.com', '11111111111', NULL, '$2y$10$IwXfRs.sofdHevl37okQKeYZqcHPbB33/.RPCrlzIV4t9gTCgd0Yi', NULL, '2020-01-21 19:44:43', '2020-01-21 19:44:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_deartments`
--

CREATE TABLE `user_deartments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_deartments`
--

INSERT INTO `user_deartments` (`id`, `user_id`, `department_id`, `created_at`, `updated_at`) VALUES
(1, 9, 3, '2020-01-09 18:37:32', '2020-01-18 11:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE `withdraws` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inventory_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catalogs`
--
ALTER TABLE `catalogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalogs_categories_id_foregin_key` (`categories_id`);

--
-- Indexes for table `catalog_items`
--
ALTER TABLE `catalog_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_items_cataglog_id_foreign` (`cataglog_id`),
  ADD KEY `catalog_inventory` (`inventory_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`),
  ADD KEY `customers_user_id_foregin_key` (`user_id`);

--
-- Indexes for table `customer_files`
--
ALTER TABLE `customer_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_files_user_data_id_foreign` (`customer_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory_user` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `order__reviews`
--
ALTER TABLE `order__reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order__reviews_order_id_foreign` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soical_media`
--
ALTER TABLE `soical_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_customer_id_foreign` (`customer_id`),
  ADD KEY `tasks_user_id_foreign` (`user_id`),
  ADD KEY `tasks_order_id_foreign` (`order_id`),
  ADD KEY `tasks_catalog_id_foreign` (`catalog_id`),
  ADD KEY `tasks_department_id_foreign` (`department_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_deartments`
--
ALTER TABLE `user_deartments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_deartments_user_id_foreign` (`user_id`),
  ADD KEY `user_deartments_department_id_foreign` (`department_id`);

--
-- Indexes for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catalogs`
--
ALTER TABLE `catalogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `catalog_items`
--
ALTER TABLE `catalog_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_files`
--
ALTER TABLE `customer_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order__reviews`
--
ALTER TABLE `order__reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `soical_media`
--
ALTER TABLE `soical_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_deartments`
--
ALTER TABLE `user_deartments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `catalogs`
--
ALTER TABLE `catalogs`
  ADD CONSTRAINT `catalogs_categories_id_foregin_key` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `catalog_items`
--
ALTER TABLE `catalog_items`
  ADD CONSTRAINT `catalog_inventory` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_items_cataglog_id_foreign` FOREIGN KEY (`cataglog_id`) REFERENCES `catalogs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_user_id_foregin_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_files`
--
ALTER TABLE `customer_files`
  ADD CONSTRAINT `user_files_user_data_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inventories`
--
ALTER TABLE `inventories`
  ADD CONSTRAINT `inventory_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order__reviews`
--
ALTER TABLE `order__reviews`
  ADD CONSTRAINT `order__reviews_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_catalog_id_foreign` FOREIGN KEY (`catalog_id`) REFERENCES `catalogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_deartments`
--
ALTER TABLE `user_deartments`
  ADD CONSTRAINT `user_deartments_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_deartments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
