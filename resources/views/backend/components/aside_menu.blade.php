
<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
	<div id="kt_aside_menu" class="kt-aside-menu " style="overflow-y:hidden" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
		<ul class="kt-menu__nav ">
			<li class="kt-menu__item  @yield('homesactive')" aria-haspopup="true"><a href="{{ route('dashboard_index') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-architecture-and-city" style="color:#f5f6ff;"></i><span class="kt-menu__link-text">@lang('tr.Dashboard')</span></a></li>
			<li class="kt-menu__section ">
				<h4 class="kt-menu__section-text">@lang('tr.Users')</h4>
				<i class="kt-menu__section-icon flaticon-more-v2"></i>
			</li>

			<li class="kt-menu__item  @yield('usersactive')" aria-haspopup="true"><a href="{{ route('users') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-users" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Users')</span></a></li>
			<li class="kt-menu__item  @yield('customersactive')" aria-haspopup="true"><a href="{{ route('customers') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-users" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Customers')</span></a></li>
			<li class="kt-menu__item  @yield('departmentsactive')" aria-haspopup="true"><a href="{{ route('departments') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-user-tag" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Departments')</span></a></li>

			
			
			
			<li class="kt-menu__section ">
				<h4 class="kt-menu__section-text">@lang('tr.Items')</h4>
				<i class="kt-menu__section-icon flaticon-more-v2"></i>
			</li>

			<li class="kt-menu__item   @yield('inventorysactive')" aria-haspopup="true"><a href="{{ route('inventory') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-database" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Inventory')</span></a></li>
			<li class="kt-menu__item   @yield('categoriesactive')" aria-haspopup="true"><a href="{{ route('category') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa flaticon2-tag" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Categories')</span></a></li>
			<li class="kt-menu__item   @yield('catalogssactive')" aria-haspopup="true"><a href="{{ route('catalogs') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa flaticon-layers" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Catalogs')</span></a></li>
			<li class="kt-menu__item   @yield('ordersactive')" aria-haspopup="true"><a href="{{ route('orders') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-file-invoice" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Orders')</span></a></li>
			<li class="kt-menu__item   @yield('tasksactive')" aria-haspopup="true"><a href="{{ route('tasks') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-tasks" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Tasks')</span></a></li>
			{{-- <li class="kt-menu__item   @yield('profitsactive')" aria-haspopup="true"><a href="{{ route('profits') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-money-check" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.Profits')</span></a></li> --}}




			<li class="kt-menu__section ">
				<h4 class="kt-menu__section-text">setting</h4>
				<i class="kt-menu__section-icon flaticon-more-v2"></i>
			</li>

			<li class="kt-menu__item   @yield('contactssactive')" aria-haspopup="true"><a href="{{ route('contactus') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-phone" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.contact us')</span></a></li>

			<li class="kt-menu__item   @yield('settingsactive')" aria-haspopup="true"><a href="{{ route('settings_index') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-cogs" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.settings')</span></a></li>
			<li class="kt-menu__item   @yield('socialsactive')" aria-haspopup="true"><a href="{{ route('social_media_index') }}" class="kt-menu__link "><i class="kt-menu__link-bullet fa fa-link" style="color:#f5f6ff;"><span></span></i>&nbsp;&nbsp;&nbsp;<span class="kt-menu__link-text">@lang('tr.social_media')</span></a></li>


			

		</ul>
	</div>
</div>

<!-- end:: Aside Menu -->