@extends('backend.layouts.master')

@section('title',__('tr.All Users'))

@section('usersactive','kt-menu__item  kt-menu__item--active')
    
@section('stylesheet')
    
@endsection

@section('content')
    

<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.All Users')
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    @can('create_users')
                    <a href="{{ route('create_users') }}" target="_blank" class="btn btn-primary">@lang('tr.Create New Users')</a>
                    @endcan
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="col-xl-12 order-lg-2 order-xl-1">
                    
                    <table id="example" class="display" style="width:100%;" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Name')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Email')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Mobile')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Department')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $index => $user)
                            <tr>
                                <td style="border:1px solid #eee;padding:10px;">{{ $index+1 }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $user->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $user->email }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $user->mobile }}</td>
                                @if($user->hasDepartment($user->id) > 0)
                                <td style="border:1px solid #eee;padding:10px;">{{ $user->departmentName($user->id)->name }}</td>
                                @else
                                <td>@lang('tr.No Department')</td>
                                @endif
                                <td style="border:1px solid #eee;padding:10px;">
                                    
                                    @can('show_users')
                                    <a href="{{ route('show_users',$user->id) }}" style="background: orange; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.View')</a>&nbsp;
                                    @endcan

                                    @can('edit_users')
                                    <a href="{{ route('edit_users',$user->id) }}" style="background: purple; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.Edit')</a>&nbsp;
                                    @endcan

                                    @can('delete_users')
                                    <a onclick="return confirm('Are You Sure ?')" style="background: red; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;" href="{{ route('delete_users',$user->id) }}">@lang('tr.Delete')</a>
                                    @endcan

                                    @can('permission_users')
                                    <a style="background: green; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;" href="{{ route('permissions_users',$user->id) }}">@lang('tr.Permissions')</a>
                                    @endcan

                                    @can('department_users')
                                    @php($depCount = \DB::select('select count(*) as "count" from departments')[0]->count)
                                    @if($depCount > 0)
                                        @if($user->hasDepartment($user->id) == 0)
                                        <a style="background: black; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;cursor:pointer;" class="btnDep" data-user_id="{{ $user->id }}" data-toggle="modal" data-target="#exampleModalCenter">@lang('tr.Add Department')</a>
                                        @else
                                        <a style="background: black; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;cursor:pointer;" class="btnDep" data-user_id="{{ $user->id }}" data-toggle="modal" data-target="#exampleModalCenter">@lang('tr.Update Department')</a>
                                        @endif
                                    @endif
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Name')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Email')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Mobile')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Department')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="department">@lang('tr.Department')</label>
                        <form action="{{ route('assign_departments_users') }}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" class="user_id">
                            @if($user->departmentName($user->id)->name == null)
                            <select name="department" id="department" class="form-control" required>
                                <option value="">@lang('tr.Select Department')</option>
                                @foreach ($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                            @else
                            <input type="hidden" name="department_id" value="{{ $user->departmentName($user->id)->id }}">
                            <select name="department" id="department" class="form-control" required>
                                <option value="">@lang('tr.Select Department')</option>
                                @foreach ($departments as $department)
                                    @if($department->id == $user->departmentName($user->id)->id)
                                    <option value="{{ $department->id }}" selected>{{ $department->name }}</option>
                                    @else
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @endif
                            
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" value="@lang('tr.Save')" class="btn btn-primary">
        </form>
        </div>
      </div>
    </div>
  </div>
<!-- Modal -->


@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        $('.btnDep').click(function() {
            var $this = $(this);
            var user_id = $(this).data('user_id');
            $('.user_id').val(user_id);
            $this.parents('tr').prev().css("background-color", "yellow");

        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection