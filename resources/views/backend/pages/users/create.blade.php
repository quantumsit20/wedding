@extends('backend.layouts.master')

@section('title',__('tr.Create New User'))
    
@section('usersactive','kt-menu__item  kt-menu__item--active')

@section('stylesheet')
    
@endsection

@section('content')


<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.Add New User')
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="col-xl-12 order-lg-2 order-xl-1">
                  
                    @include('backend.components.errors')
                   
                <form action="{{ route('store_users') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">@lang('tr.Name')</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="@lang('tr.Enter User Name')" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="email">@lang('tr.Email')</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="@lang('tr.Enter User Email')" required>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="password">@lang('tr.Password')</label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="•••••••••" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="password_confirmation">@lang('tr.Confirm Password')</label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="•••••••••" required>
                            </div>
                        </div>
                    </div>

                    

                    
                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>&nbsp;@lang('tr.Save')
                        </button>
                    </div>
                </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>


    
@endsection

@section('javascript')

@endsection