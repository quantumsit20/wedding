@extends('backend.layouts.master')

@section('title',__('tr.Orders'))

@section('ordersactive','kt-menu__item  kt-menu__item--active')
    
@section('stylesheet')
    
@endsection

@section('content')


<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.Invoice')
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit" style="padding:20px;">
                <div id="printInvoice">
                <div class="col-xl-12 order-lg-2 order-xl-1" style="padding:20px;border: 2px solid #eee;">
                   
                    <div class="row">
                        <div class="col-lg-9">
                            <h2 style="font-weight:bold;text-transform: uppercase;margin-bottom:15px;">@lang('tr.Invoice')</h2>
                            <h4>{{ $orders->order_code }}</h4>
                            <h4>@lang('tr.Customer'): {{ $customer->name }}</h4>
                            <h4>@lang('tr.Email'): {{ $customer->email }}</h4>
                        </div>
                        <div class="col-lg-3">
                            <h3>
                                @php($lang = \App::getLocale())
                                <img alt="Logo" style="width: 200px;" class="@if($lang == 'ar') pull-left @else pull-right @endif" src="{{ asset('logo/'.$system_logo) }}">
                            </h3>
                        </div>
                    </div>

                    <br>
                    <hr>
                    
                    <div class="row" style="padding:25px;">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="text-transform: uppercase;color:#74788d;">@lang('tr.Name')</th>
                                        <th style="text-transform: uppercase;color:#74788d;">@lang('tr.Quantity')</th>
                                        <th style="text-transform: uppercase;color:#74788d;">@lang('tr.Price')</th>
                                        <th style="text-transform: uppercase;color:#74788d;">@lang('tr.Total')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($order_data = json_decode($orders->order_data,true))
                                    @php($count = count($order_data) / 4)

                                    @for ($i = 0; $i < $count; $i++)
                                        @php($title = 'title_'.$i)
                                        @php($quantity = 'quantity_'.$i)
                                        @php($total = 'total_'.$i)

                                        <tr>
                                            <td style="color:#595d6e;font-weight:bold;">{{ $order_data[$title] }}</td>
                                            <td style="color:#595d6e;font-weight:bold;">{{ $order_data[$quantity] }}</td>
                                            <td style="color:#595d6e;font-weight:bold;">{{ $order_data[$total] / $order_data[$quantity].' '.$system_currency }}</td>
                                            <td style="color:#595d6e;font-weight:bold;">{{ $order_data[$total].' '.$system_currency }}</td>
                                        </tr>
                                    @endfor
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <br>
                    <div class="row" style="padding:25px;background:#efefef47;">
                        <div class="col-lg-3" style="text-align:center;">
                            <h5 style="text-transform: uppercase;color:#74788d;">@lang('tr.Day')</h5><hr>
                            <h4 style="font-weight:bold;">{{ $orders->order_day }}</h4>
                        </div>
                        <div class="col-lg-3" style="text-align:center;">
                            <h5 style="text-transform: uppercase;color:#74788d;">@lang('tr.From')</h5><hr>
                            <h4 style="font-weight:bold;">{{ $orders->order_from }}</h4>
                        </div>
                        <div class="col-lg-3" style="text-align:center;">
                            <h5 style="text-transform: uppercase;color:#74788d;">@lang('tr.To')</h5><hr>
                            <h4 style="font-weight:bold;">{{ $orders->order_to }}</h4>
                        </div>
                        <div class="col-lg-3" style="text-align:center;">
                            <h5 style="text-transform: uppercase;color:#74788d;">@lang('tr.Total')</h5><hr>
                            <h4 style="font-weight:bold;color:#fd397a;">{{ $orders->total_price }}</h4>
                        </div>
                    </div>
                </div>
                    <br>
                    <div class="row" style="padding:25px;">
                        <a  style ="color: white;" onclick="printJS({printable: 'printInvoice', type: 'html', ignoreElements: ['a', 'button']})" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</a>
                    </div>
                   
                    <hr>
                    <br>
                    <h6 style="text-align:center;">
                        

                        @can('delete_orders')
                        <a onclick="return confirm('Are You Sure ?')" style="background: red; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;" href="{{ route('delete_orders',$orders->id) }}">@lang('tr.Delete')</a>
                        @endcan
                    </h6>

                    <br>
                    
                </div>
            </div>
        </div>
    </div>
</div>


    
@endsection

@section('javascript')

<script>
function PrintElem(elem)
{
    window.print();

    return true;
}
</script>

@endsection