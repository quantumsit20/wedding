@extends('backend.layouts.master')

@section('ordersactive','kt-menu__item  kt-menu__item--active')

@section('title',__('tr.Orders'))
    
@section('stylesheet')
    
@endsection

@section('content')
    

<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.Orders')
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="col-xl-12 order-lg-2 order-xl-1">

                    <table id="example" class="display" style="width:100%;" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Code')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Customer')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Order Data')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Day')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.From') - @lang('tr.To')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Total')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Status')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $index => $order)
                            @php($order_data = json_decode($order->order_data,true))
                            @php($count = count($order_data) / 4)
                            <tr>
                                <td style="border:1px solid #eee;padding:10px;">{{ $index+1 }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $order->order_code }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $order->customer->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    @for ($i = 0; $i < $count; $i++)
                                        @php($title = 'title_'.$i)
                                        @php($quantity = 'quantity_'.$i)
                                        @php($total = 'total_'.$i)

                                        <span style="color:#f05f78;font-style:italic;font-weight: bold;">{{ $order_data[$title].', Quantity: '.$order_data[$quantity].', Total: '.$order_data[$total].' '.$system_currency }}</span><br>
                                    @endfor
                                </td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $order->order_day }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $order->order_from.' - '.$order->order_to }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $order->total_price }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    @if ($order->status == 'finished')
                                        <span style="font-weight:bold;color:green">{{ __('tr.'.$order->status) }}</span>
                                    @else
                                        {{ __('tr.'.$order->status) }}
                                    @endif
                                </td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    {{-- <a href="{{ route('reviews_orders',$order->id) }}" style="background: green; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.reviews')</a>&nbsp; --}}

                                    @can('show_orders')
                                    <a href="{{ route('show_orders',$order->id) }}" style="background: orange; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.View')</a>&nbsp;
                                    @endcan

                                    @can('edit_orders')
                                    @if($order->editOrdeleteOrder($order->id) == 0)
                                        @if($order->expiredOrder($order->id) != 'finished')
                                            
                                        @endif
                                    @endif
                                    @endcan

                                    @can('delete_orders')
                                    @if($order->editOrdeleteOrder($order->id) == 0)
                                        @if($order->expiredOrder($order->id) != 'finished')
                                            
                                        @endif
                                    @endif
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Code')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Customer')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Order Data')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Day')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.From')&nbsp;@lang('tr.To')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Total')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Status')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('javascript')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection