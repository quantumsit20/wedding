@extends('backend.layouts.master')

@section('inventorysactive','kt-menu__item  kt-menu__item--active')

@section('title',__('tr.Inventory'))
    
@section('stylesheet')
    
@endsection

@section('content')
    

<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.Inventory')
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    @can('create_inventory')
                    <a href="{{ route('create_inventory') }}" target="_blank" class="btn btn-primary">@lang('tr.Create New Items')</a>
                    @endcan
                    @can('withdraw_inventory')
                    &nbsp;<a href="{{ route('withdraw_inventory') }}" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">@lang('tr.Withdraw Items')</a>
                    @endcan
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="col-xl-12 order-lg-2 order-xl-1">
                    
                    <table id="example" class="display" style="width:100%;" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Name')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Quantity')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Price')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Original Price')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Added Value')</th>
                                {{-- <th style="border:1px solid #eee;padding:10px;">@lang('tr.Total')</th> --}}
                                {{-- <th style="border:1px solid #eee;padding:10px;">@lang('tr.Original Total')</th> --}}
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.User')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Notes')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($inventory as $index => $item)
                            <tr>
                                <td style="border:1px solid #eee;padding:10px;">{{ $index+1 }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    @if($item->quantity <= 0)
                                    <span style="color:red;font-weight:bold">{{ $item->name }}</span>
                                    @else
                                    {{ $item->name }}
                                    @endif
                                </td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    @if($item->quantity <= 0)
                                    <span style="color:red;font-weight:bold">{{ $item->quantity }}</span>
                                    @else
                                    {{ $item->quantity }}
                                    @endif
                                </td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->price }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->orignal_price }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->add_value }}</td>
                                {{-- <td style="border:1px solid #eee;padding:10px;">{{ $item->total_price }}</td> --}}
                                {{-- <td style="border:1px solid #eee;padding:10px;">{{ $item->total_orignal_price }}</td> --}}
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->user->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->notes }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    
                                    @can('show_inventory')
                                    <a href="{{ route('show_inventory',$item->id) }}" style="background: orange; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.View')</a>&nbsp;
                                    @endcan

                                    @can('edit_inventory')
                                    <a href="{{ route('edit_inventory',$item->id) }}" style="background: purple; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.Edit')</a>&nbsp;
                                    @endcan

                                    @can('delete_inventory')
                                    <a onclick="return confirm('Are You Sure ?')" style="background: red; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;" href="{{ route('delete_inventory',$item->id) }}">@lang('tr.Delete')</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Name')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Quantity')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Price')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Original Price')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Added Value')</th>
                                {{-- <th style="border:1px solid #eee;padding:10px;">@lang('tr.Total')</th> --}}
                                {{-- <th style="border:1px solid #eee;padding:10px;">@lang('tr.Original Total')</th> --}}
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.User')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Notes')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>


{{-- Withdraw --}}
<!-- Button trigger modal -->
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
            <form action="{{ route('withdraw_inventory') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="item_inv">@lang('tr.Item')</label>
                    <select name="item_inv" id="item_inv" class="form-control" required>
                        <option value="">@lang('tr.Select Items')</option>
                        @foreach ($inventory as $inv)
                            <option value="{{ $inv->id }}">{{ $inv->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="invID"></div>

                <hr>

                <div class="form-group">
                    <label for="customer_inv">@lang('tr.Customer')</label>
                    <select name="customer_inv" id="customer_inv" class="form-control" required>
                        <option value="">@lang('tr.Select Customer')</option>
                        @foreach ($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endforeach
                    </select>
                </div>
            

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;@lang('tr.Save')</button>
        </form>
        </div>
      </div>
    </div>
  </div>
{{-- Withdraw --}}



@endsection

@section('javascript')
<script>
    
    $(document).ready(function() {

        var invUrl = '{{ route("withdraw_items",["id"=>"#id"]) }}';
        $('#item_inv').change(function(){
            var itemID = $(this).val();
            if (itemID != '') {
                invUrl = invUrl.replace('#id',itemID);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'GET',
                    url:invUrl,
                    data:{},
                        success:function(data){
                            $('#invID').html('<input type="number" onkeypress="validate(event)" placeholder="@lang("tr.Maximum Value is") '+data.inv.quantity+'" value="1" step="1" min="1" max="'+data.inv.quantity+'" class="form-control" required name="inv_quantity">');
                    }
                });
            }else{
                $('#invID').html('');
            }
        });

        $('#example').DataTable();
    } );

    
</script>

<script>
    function validate(evt) {
      var theEvent = evt || window.event;
    
      // Handle paste
      if (theEvent.type === 'paste') {
          key = event.clipboardData.getData('text/plain');
      } else {
      // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }
    </script>
@endsection