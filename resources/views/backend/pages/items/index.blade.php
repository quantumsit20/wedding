@extends('backend.layouts.master')

@section('itemsactive','kt-menu__item  kt-menu__item--active')

@section('title',__('tr.All Items'))
    
@section('stylesheet')
    
@endsection

@section('content')
    

<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.All Items')
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    @can('create_items')
                    <a href="{{ route('create_items') }}" target="_blank" class="btn btn-primary">@lang('tr.Create New Items')</a>
                    @endcan
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="col-xl-12 order-lg-2 order-xl-1">
                    
                    <table id="example" class="display" style="width:100%;" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Name')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Price')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Quantity')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Total')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Catalog')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $index => $item)
                            <tr>
                                <td style="border:1px solid #eee;padding:10px;">{{ $index+1 }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->price }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->quantity }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->total_price }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $item->catalog($item->cataglog_id)->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    
                                    @can('show_items')
                                    <a href="{{ route('show_items',$item->id) }}" style="background: orange; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.View')</a>&nbsp;
                                    @endcan

                                    @can('edit_items')
                                    <a href="{{ route('edit_items',$item->id) }}" style="background: purple; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.Edit')</a>&nbsp;
                                    @endcan

                                    @can('delete_items')
                                    <a onclick="return confirm('Are You Sure ?')" style="background: red; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;" href="{{ route('delete_items',$item->id) }}">@lang('tr.Delete')</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Name')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Descriptions')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Image')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Price')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Catalog')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('javascript')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection