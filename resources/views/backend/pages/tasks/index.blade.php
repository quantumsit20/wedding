@extends('backend.layouts.master')

@section('tasksactive','kt-menu__item  kt-menu__item--active')

@section('title',__('tr.Tasks'))
    
@section('stylesheet')
    
@endsection

@section('content')
    

<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        @lang('tr.Tasks')
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    @can('create_tasks')
                    <a href="{{ route('create_tasks') }}" target="_blank" class="btn btn-primary">@lang('tr.Create New Task')</a>
                    @endcan
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="col-xl-12 order-lg-2 order-xl-1">

                    <table id="example" class="display" style="width:100%;" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Task Title')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Task Date')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Customer')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Catalog')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.User')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Department')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Status')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Notes')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tasks as $index => $task)
                            <tr>
                                <td style="border:1px solid #eee;padding:10px;">{{ $index+1 }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->task_title }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->task_date }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->customer->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->catalogName($task->catalog_id)->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->user->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->departmentName($task->department_id)->name }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    {{ $task->taskStatus($task->status) }}
                                </td>
                                <td style="border:1px solid #eee;padding:10px;">{{ $task->notes }}</td>
                                <td style="border:1px solid #eee;padding:10px;">
                                    
                                    @can('show_tasks')
                                    <a href="{{ route('show_tasks',$task->id) }}" style="background: orange; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.View')</a>&nbsp;
                                    @endcan

                                    @can('edit_tasks')
                                    {{-- <a href="{{ route('edit_tasks',$task->id) }}" style="background: purple; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;">@lang('tr.Edit')</a>&nbsp; --}}
                                    @endcan

                                    @can('delete_tasks')
                                    <a onclick="return confirm('Are You Sure ?')" style="background: red; padding: 5px 10px 5px 10px; border-radius: 20px; color: white;" href="{{ route('delete_tasks',$task->id) }}">@lang('tr.Delete')</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="border:1px solid #eee;padding:10px;">#</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Task Title')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Task Date')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Customer')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Catalog')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.User')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Department')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Status')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Notes')</th>
                                <th style="border:1px solid #eee;padding:10px;">@lang('tr.Action')</th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('javascript')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection