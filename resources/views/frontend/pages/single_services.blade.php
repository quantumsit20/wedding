@extends('frontend.layouts.master')

@section('title',$catalog->name)

@section('servicesactive','current')

@section('stylesheet')

<link rel="stylesheet" href="{{ asset('frontend/font/linearicons/demo.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/font/fontello/fontello.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/plugins/fancybox/jquery.fancybox.css')}}">

<!-- CSS theme files
============================================ -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap-grid.min.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/css/rtl.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css')}}">
    
@endsection

@section('content')

@include('frontend.components.breadcrumb')

<div id="content" class="page-content-wrap">

      <div class="container wide">
        
        <div class="content-element8">
            
          <div class="product single">

            <div class="row">

              <div class="col-lg-6">

                <div class="image-preview-container">

                  <!-- - - - - - - - - - - - - - Image Preview Container - - - - - - - - - - - - - - - - -->

                  <div class="image-preview">
                    
                    <a href="{{ asset('catalogs/'.$catalog->catalog_img) }}" data-zoom-image="{{ asset('catalogs/'.$catalog->catalog_img) }}" data-fancybox="group">
                      <img id="zoom-image" style="width:100%;height:100%;" src="{{ asset('catalogs/'.$catalog->catalog_img) }}" alt="">
                    </a>
                  </div><!--/ .image-preview-->

                  <!-- - - - - - - - - - - - - - End of Image Preview Container - - - - - - - - - - - - - - - - -->

                  <!-- - - - - - - - - - - - - - Thumbnails - - - - - - - - - - - - - - - - -->


                  <!-- - - - - - - - - - - - - - End of Thumbnails - - - - - - - - - - - - - - - - -->

                </div><!--/ .image-preview-container -->

              </div>

              <div class="col-lg-6">

                <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->

                <div class="product-description">

                  <h1 class="product-name">{{ $catalog->name }}</h1>

                  <div class="pricing-area">

                    <ul class="rating size-2">
                      <li><i class="licon-star"></i></li>
                      <li><i class="licon-star"></i></li>
                      <li><i class="licon-star"></i></li>
                      <li><i class="licon-star"></i></li>
                      <li class="empty"><i class="licon-star"></i></li>
                    </ul>

                    &nbsp;&nbsp;

                    <span>(<a href="#" class="link">2 customer reviews</a>)</span>
                    
                    
                  </div>

                  <hr>
                    <div class="shop-cart-form table-type-1 responsive-table">
                      
                      <table>
                          <thead>
                              <th style="text-align: center; color: #db1430; font-size: 15px;">@lang('tr.Name')</th>
                              <th style="text-align: center; color: #db1430; font-size: 15px;">@lang('tr.Price')</th>
                              <th style="text-align: center; color: #db1430; font-size: 15px;">@lang('tr.Cart')</th>
                          </thead>
                          <tbody>
                              @foreach ($items as $item)
                              <tr style="font-weight:bold;text-align:center;">
                                  <td style="text-align:center">{{ $item->name }}</td>
                                  <td style="text-align:center">{{ $item->price.' '.$system_currency }}</td>
                                  <form action="{{ route('frontend_add_to_cart') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="item_id" value="{{ $item->id }}">  
                                  </td>
                                  <td style="text-align:center">
                                    <button type="submit" style="background:transparent;border:0;"><i class="fa fa-cart-plus" style="color:#f05f79;font-size:20px;"></i></button>  
                                </form>
                                    
                                  
                              </tr>
                              @endforeach
                          </tbody>
                      </table>

                  </div>

                
                  
                </div>

                <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->

              </div>

            </div><!--/ .row -->

          </div><!--/ .product-single -->

        </div>

        <div class="content-element8">
          
          <div class="tabs tabs-section">
            <!--tabs navigation-->                  
            <ul class="tabs-nav">
              <li>
                <a href="#tab-1">@lang('tr.Descriptions')</a>
              </li>
              <li>
                <a href="#tab-2">@lang('tr.Reviews') (2)</a>
              </li>
            </ul>
            <!--tabs content-->                 
            <div class="tabs-content">
              <div id="tab-1">

                <p>
                  {{ $catalog->desc }}
                </p>

              </div>
              <div id="tab-2">

                <div class="row">
                  <div class="col-lg-6">
                    
                    <h5 class="title-large style-2">2 Reviews For Classic Cake</h5>

                    <ol class="comments-list">

                      <li class="comment">

                        <article>

                          <!-- - - - - - - - - - - - - - Avatar - - - - - - - - - - - - - - - - -->

                          <div class="gravatar">
                            
                            <a href="#"><img src="images/84x84_photo6.jpg" alt=""></a>

                          </div>

                          <!-- - - - - - - - - - - - - - End of avatar - - - - - - - - - - - - - - - - -->

                          <!-- - - - - - - - - - - - - - Comment body - - - - - - - - - - - - - - - - -->

                          <div class="comment-body">

                            <header class="comment-meta">

                              <h6 class="comment-author"><a href="#">Camala Haddon</a></h6>
                              <div class="comment-info justify-content-between">
                                <time datetime="2018-10-17 02:41" class="comment-date">October 17, 2018 at 2:41 pm</time>
                                <ul class="rating">
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                  <li class="empty"><i class="licon-star"></i></li>
                                </ul>
                              </div>

                            </header>

                            <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>

                          </div><!--/ .comment-body-->

                          <!-- - - - - - - - - - - - - - End of comment body - - - - - - - - - - - - - - - - -->

                        </article>

                      </li>

                      <li class="comment">

                        <article>

                          <!-- - - - - - - - - - - - - - Avatar - - - - - - - - - - - - - - - - -->

                          <div class="gravatar">
                            
                            <a href="#"><img src="images/84x84_photo2.jpg" alt=""></a>

                          </div>

                          <!-- - - - - - - - - - - - - - End of avatar - - - - - - - - - - - - - - - - -->

                          <!-- - - - - - - - - - - - - - Comment body - - - - - - - - - - - - - - - - -->

                          <div class="comment-body">

                            <header class="comment-meta">

                              <h6 class="comment-author"><a href="#">Bradley Grosh</a></h6>
                              <div class="comment-info flex-row justify-content-between">
                                <time datetime="2018-10-17 02:41" class="comment-date">October 17, 2018 at 2:41 pm</time>
                                <ul class="rating">
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                  <li><i class="licon-star"></i></li>
                                </ul>
                              </div>

                            </header>

                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>

                          </div><!--/ .comment-body-->

                          <!-- - - - - - - - - - - - - - End of comment body - - - - - - - - - - - - - - - - -->

                        </article>

                      </li>
                  
                    </ol>

                  </div>
                  <div class="col-lg-6">
                    
                    <h5 class="title-large style-2">Add a Review</h5>
                    <p class="content-element1">Your email address will not be published. </p>
                    <div class="rsvp-form no-bg">
                
                      <form class="contact-form">
                        
                        <div class="row">
                          
                          <div class="col-12">
                            
                            <div class="input-box">
                              
                              <input type="text" required="">
                              <label>Your Name</label>

                            </div>

                            <div class="input-box">
                              
                              <input type="email" required="">
                              <label>Your Email</label>

                            </div>

                            <div class="input-box">
                  
                              <label class="var2">Rating</label>
                              <ul class="rating size-3">
                                <li><i class="licon-star"></i></li>
                                <li><i class="licon-star"></i></li>
                                <li><i class="licon-star"></i></li>
                                <li><i class="licon-star"></i></li>
                                <li class="empty"><i class="licon-star"></i></li>
                              </ul>
                        
                            </div>

                            <div class="input-box">
                              
                              <input type="text" required="">
                              <label>Comments (optional)</label>

                            </div>

                            <div class="input-box">
                              
                              <button type="submit" class="btn btn-style-2">Submit review</button>

                            </div>

                          </div>

                        </div>

                      </form>

                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>

        </div>

        

      </div>
      
    </div>

    @endsection

@section('javascript')
  <script src="{{asset('frontend/plugins/instafeed.min.js')}}"></script>
  <script src="{{asset('frontend/plugins/elevatezoom.min.js')}}"></script>
  <script src="{{asset('frontend/plugins/mad.customselect.js')}}"></script>
  <script src="{{asset('frontend/plugins/fancybox/jquery.fancybox.min.js')}}"></script>
  <script src="{{asset('frontend/plugins/jquery.queryloader2.min.js')}}"></script>

  <script>
  $('#order_from').on("input",function(){
        if($(this).val() != ''){
           var times = $(this).val().split(':');
            
            $('#order_to').removeAttr('readonly');
            $('#order_to').attr('min',times[0]+":"+times[1]+":00");
            $('#order_to').val(times[0]+":"+times[1]+":00");
        }else{
            $('#order_to').attr('readonly');
        }
    });
    

    $('#order_to').on("input",function(){
        if($(this).val() != ''){
            var timesTo = $(this).val().split(':');
            var timesFrom = $('#order_from').val().split(':');
           if(timesTo[0] < timesFrom[0] || timesTo[1] < timesFrom[1]){
                    $(this).val($('#order_from').val());
           }
        }else{
            $(this).val($('#order_from').val());
        }
    });

    $('#order_day').on("input",function(){
        if($(this).val() != ''){
            var d = '{{ date("Y-m-d") }}';
           if($(this).val() < d){
                $(this).val(d);
           }
        }
    });
  </script>

<script>
  $(document).ready(function() {
      var max_fields      = 100;
      var wrapper         = $(".container1"); 
      var add_button      = $(".add_form_field"); 
      var skillHtml       = '';
      var x = 1; 
      $(add_button).click(function(e){ 
          e.preventDefault();
          if(x < max_fields){ 
              x++;
              skillHtml += '<div style="border: 1px solid #e4e4e4; padding: 15px;margin-top:10px;">';
              skillHtml += '<div class="row">';
              skillHtml += '<div class="col-lg-4"><div class="form-group"><label for="follow_name">@lang("tr.Follower Name")</label><input type="text" name="follow_name[]" id="follow_name" class="form-control" placeholder="@lang("tr.Follower Name")" required></div></div>';
              skillHtml += '<div class="col-lg-4"><div class="form-group"><label for="follow_mobile">@lang("tr.Follower Mobile")</label><input type="text" name="follow_mobile[]" id="follow_mobile" class="form-control" placeholder="@lang("tr.Follower Mobile")" required></div></div>';
              skillHtml += '<div class="col-lg-4"><div class="form-group"><label for="follow_email">@lang("tr.Follower Email")</label><input type="email" name="follow_email[]" id="follow_email" class="form-control" placeholder="@lang("tr.Follower Email")" required></div></div>';
              skillHtml += '</div>';


              skillHtml += '<br><a href="#" class="delete btn btn-danger"><i class="fa fa-trash"></i></a></div>'; //add input box


              $(wrapper).append(skillHtml);

              skillHtml = '';
              $('.save_btn').html('<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp; @lang("tr.Save")</button>');
          }
          else
          {
          alert('You Reached the limits')
          }
      });
      
      $(wrapper).on("click",".delete", function(e){ 
          e.preventDefault(); $(this).parent('div').remove(); x--;
      })
  });
</script>

@endsection