@extends('frontend.layouts.master')

@section('title',__('tr.Our Services'))

@section('servicesactive','current')

@section('stylesheet')
    
@endsection

@section('content')

@include('frontend.components.breadcrumb')

<div id="content" class="page-content-wrap">

  <div class="container wide">

    <div class="col-lg-12 col-md-12">

      <div class="content-element4 ">

        <div class="align-center marginBottom">
          <h2 class="title-large style-2">@lang('tr.Make your day is nice from start to end')</h2>
          <p>@lang('tr.We offer you everything you need for occasions')</p>
        </div>

      </div>

        <div class="tabs style-2 tabs-section">
          
          <!--tabs navigation-->                  
          <ul class="tabs-nav clearfix">

            @foreach ($category as $cat)
              <li>
                <a href="#tab-{{ $cat->id }}">{{ $cat->name }}</a>
              </li>
            @endforeach

            
          </ul>

          
        <div class="row">

          <div class="col-lg-9">
            <div class="tabs-content">
  
              <div class="product-sort-section">
        
                {{-- <span>1–9 of {{ \App\Catalog::count() }} @lang('tr.Catalogs')</span> --}}
  
                <div class="mad-custom-select">
                  <select data-default-text="Default sorting">
                    <option value="Option 1">Option 1</option>
                    <option value="Option 2">Option 2</option>
                    <option value="Option 3">Option 3</option>
                  </select>
                </div>
  
              </div>
  
              @foreach ($category as $cat)
  
              <div id="tab-{{ $cat->id }}">
  
                <div class="row row-size-2">
        
                  <main id="main" class="col-lg-9">
                    
                    <!-- Product sorting -->
                    
                    
                    <div class="products-holder item-col-3">
                      @php($catalogs = \App\Catalog::getCatalogs($cat->id))
                      <!-- Product -->
                      @foreach ($catalogs as $catalog)
  
                      @if ($catalog->categories_id == $cat->id)
  
                      <div class="product">
                              
                        <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->
                        <figure class="product-image">
                          <a href="{{ route('frontend_single_services',$catalog->id) }}"><img src="{{ asset('catalogs/'.$catalog->catalog_img) }}" alt=""></a>
                        </figure>
                        <!-- - - - - - - - - - - - - - End of Product Image - - - - - - - - - - - - - - - - -->
                    
                        <!-- - - - - - - - - - - - - - Product Description - - - - - - - - - - - - - - - - -->
                        <div class="product-description">
                    
                          <h5 class="product-name"><a href="{{ route('frontend_single_services',$catalog->id) }}">{{ $catalog->name }}</a></h5>
                    
                          <div class="pricing-area">
                    
                            
                    
                          </div>
                    
                        </div>
                        <!-- - - - - - - - - - - - - - End of Product Description - - - - - - - - - - - - - - - - -->
                    
                      </div>
                          
                      @endif
                          
                      @endforeach
  
                      
                      
                      
                      <!-- Product -->
        
                    </div>
        
                    <ul class="pagination justify-content-center">
                      {{ $catalogs->links() }}
                    </ul>
        
                  </main>
                  
        
                </div>
  
              </div>
  
              @endforeach
              
            </div>
           </div>
  
            <aside id="sidebar" class="col-lg-3">
                    
              <!-- Widget search -->
              <div class="widget">
                
                <form action="" method="POST" class="newsletter style-2 type-2">
                  @csrf
                  <input type="text" name="search" placeholder="Search">
                  <button type="submit" name="searchBtn" data-type="submit"><i class="licon-magnifier"></i></button>
                
  
              </div>
  
              <!-- Widget categories -->
              <div class="widget">
              
                <h6 class="widget-title">@lang('tr.Categories')</h6>
                <ul class="custom-list">
                  @foreach ($category as $cat)
                  <li><a href="#" class="link">{{ $cat->name }}</a></li>
                  @endforeach
  
                </ul>
  
              </div>
  
              <!-- Widget filter -->
              <div class="widget">
              
                <h6 class="widget-title">@lang('tr.Filter By Price')</h6>
                <!--price-->
                <fieldset class="price-scale">
                  <div id="price"></div>
                  <div class="clearfix range-values">
                    <div class="f-left">
                      <span>@lang('tr.Price'): </span>
                      <input class="first-limit" name="from" readonly type="text" value="$1,000">
                      <span>-</span>
                      <input class="last-limit" name="to" readonly type="text" value="$99,000">
                    </div>
                    <div class="f-right">
                      <button type="submit" name="filterBtn" data-type="submit" class="btn btn-small f-right">@lang('tr.Filter')</button>
                    </div>
                  </div>
                </fieldset>
              </form>
              </div>
  
              
             
            </aside>

        </div>

    </div>

  </div>

</div>

@endsection

@section('javascript')
    
@endsection