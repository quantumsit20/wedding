@extends('frontend.layouts.master')

@section('title',__('tr.Cart'))

@section('cartsactive','current')

@section('stylesheet')

@endsection

@section('content')

@include('frontend.components.breadcrumb')

<!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

<div id="content" class="page-content-wrap">

    <div class="container wide">
      
      <div class="content-element8">
        
        <div class="shop-cart-form table-type-1 responsive-table">
          @if($cart)
          @php($total = 0)
          <table>
            <thead>
              <tr>
                <th>@lang('tr.Item')</th>
                <th>@lang('tr.Quantity')</th>
                <th>@lang('tr.Price')</th>
                <th>@lang('tr.Total')</th>
                <th>@lang('tr.Action')</th>
              </tr>
            </thead>
            <form action="{{ route('frontend_checkout_cart') }}" method="post">
              @csrf
            @foreach ($cart->items as $product)
                <tr>
                    @php($maxQty = \App\Cart::getMaxQty($product['id']))
                    <td>{{ $product['title'] }}</td>
                    <td><input type="number" name="quantity[]" class="form-control" value="{{ $product['quantity'] }}" min="1" max="{{ $maxQty }}" step="1" style="text-align: center; font-weight: bold; color: #f05f79;" required></td>
                    <td>{{ $product['price'].' '.$system_currency }}</td>
                    <td>{{ $product['quantity'] * $product['price'].' '.$system_currency }}</td>
                    @php($total = $total + ($product['quantity'] * $product['price']))
                    <td><a href="{{ route('frontend_remove_cart_item',$product['id']) }}" onclick="return confirm('Are You Sure ?')"><i class="fa fa-trash"></i></a></td>
                    <input type="hidden" name="ids[]" value="{{ $product['id'] }}">
                    <input type="hidden" name="title[]" value="{{ $product['title'] }}">
                    <input type="hidden" name="price[]" value="{{ $product['price'] }}">
                </tr>
            @endforeach


            <tr>
                <td colspan="3">
                  
                </td>
                <td colspan="2">
                  <button type="submit" class="btn btn-success" style="float: left;"><i class="fa fa-money-bill"></i>&nbsp;@lang('tr.Checkout')</button>
                </td>
              </tr>
            </form>
           
          </table>
          @else
            <h1 style="text-align: center; padding: 20px; color: #f05f79;font-size:70px;"><i class="fas fa-exclamation-triangle"></i></h1>
            <h2 style="text-align: center; padding: 20px; color: #f05f79;">@lang('tr.There are no items')</h2>
          @endif
        </div>

      </div>

      

    </div>
    
  </div>

  <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

@endsection

@section('javascript')

@endsection