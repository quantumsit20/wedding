<!DOCTYPE html>
<html lang="en">

@include('frontend.components.head')

<body>
    <!-- - - - - - - - - - - - - -  start feedback  - - - - - - - - - - - - - - - - -->
    <div id="feedback-main">   
        <div id="feedback-div">

        <form action="contact.php" method="post" class="form" id="feedback-form1" name="form1" enctype="multipart/form-data">
            
            <a onclick="toggle_visibility()" style="cursor:pointer;float: left; margin-bottom: 20px; font-size: 34px; font-weight: bold; color: #f05f79;">x</a>

            <p class="name">
            <input name="name" type="name" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input feedback-height" required placeholder="@lang('tr.Name')" id="feedback-name" />
            </p>
    
            <p class="email">
            <input name="email" type="email" class="validate[required,custom[email]] feedback-input feedback-height" id="feedback-email" placeholder="@lang('tr.Email')" required />
            </p>
    
            <p class="text">
            <textarea name="comment" type="comment" class="validate[required,length[6,300]] feedback-input" id="feedback-comment" required placeholder="@lang('tr.Comment')"></textarea>
            </p>
    
            <div class="feedback-submit">
            <input type="submit" value="@lang('tr.Send')" id="feedback-button-blue" />
            </div>
        </form>
        </div>
    </div>
  
    <button id="popup" class="feedback-button" onclick="toggle_visibility()"> @lang('tr.Feedback') </button>
 
  <!-- - - - - - - - - - - - - -  end feedback  - - - - - - - - - - - - - - - - -->

    <div id="loader" class="loader"></div>

    <div id="wrapper" class="wrapper-container">

    @include('frontend.components.header')

    @yield('content')
    
    </div>



    @include('frontend.components.footer')

    @include('frontend.components.scripts')
</body>
</html>