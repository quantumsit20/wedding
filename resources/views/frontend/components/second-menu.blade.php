<div class="secondmenu">
        
    <div class="container wide">
      
      <ul >

        <li><i class="fas fa-comments fa-2x"></i><a class="hints" href="#">@lang('tr.The ability to respond and communicate')</a></li>
        <li><i class="fas fa-cogs fa-2x"></i><a  class="hints" href="#">@lang('tr.Occasions Management')</a></li>
        <li><i class="fas fa-tasks fa-2x"></i><a  class="hints" href="#">@lang('tr.Orgainze Occasions')</a></li>
        <li><i class="fas fa-history fa-2x"></i><a  class="hints" href="#">@lang('tr.24 hours service')</a></li>
        <li><i class="fas fa-file-signature fa-2x"></i><a  class="hints" href="#">@lang('tr.Contract Guarantee')</a></li>
        
      </ul>

    </div>

  </div>